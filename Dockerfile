FROM golang:1.24-alpine as builder

RUN apk add --no-cache git make
WORKDIR /app

COPY . .

RUN make build

FROM alpine

COPY --from=builder /app/topology-service /app/topology-service
COPY scripts/docker-entrypoint.sh /app/entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]
