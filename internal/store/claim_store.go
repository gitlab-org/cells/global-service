package store

import (
	"context"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
)

type ClaimStore interface {
	Create(ctx context.Context, claimDetails *proto.ClaimDetails) (*proto.ClaimInfo, error)
}
