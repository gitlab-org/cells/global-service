package null_store

import (
	"context"
	"time"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
	"gitlab.com/gitlab-org/cells/topology-service/internal/store"
)

type nullClaimStore struct {
	cfg *config.Config
}

func NewClaimStore(cfg *config.Config) store.ClaimStore {
	return &nullClaimStore{cfg: cfg}
}

func (n *nullClaimStore) Create(_ context.Context, claimDetails *proto.ClaimDetails) (*proto.ClaimInfo, error) {
	var claimInfo = &proto.ClaimInfo{
		Id:       time.Now().UnixNano(),
		Details:  claimDetails,
		CellInfo: n.cfg.Cells[0].Proto(),
	}

	return claimInfo, nil
}
