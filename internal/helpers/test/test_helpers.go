package test_helpers

import (
	"os"
	"testing"

	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
)

func TestConfig(t *testing.T) *config.Config {
	configFile := os.Getenv("CONFIG_FILE")
	if configFile == "" {
		t.Fatal("The CONFIG_FILE is not set to define config file to use for testing")
	}

	cfg, err := config.ReadFile(configFile)
	if err != nil {
		t.Fatal(err)
	}

	cfg.Cells = []config.CellConfig{
		{
			Id:            100,
			Address:       "cell-1.example.com",
			SessionPrefix: "cell-1",
			SequenceRange: [2]int64{4398046511104, 8796093022207},
		},
		{
			Id:            102,
			Address:       "cell-2.example.com",
			SessionPrefix: "cell-2",
			SequenceRange: [2]int64{4398046511104, 8796093022207},
		},
	}
	return cfg
}
