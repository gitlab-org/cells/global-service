package grpc_helpers

import (
	"strings"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
)

const RawHeaderPrefix = "rawheader-"

func RawHeaderMatcher(key string) (string, bool) {
	if strings.HasPrefix(key, RawHeaderPrefix) {
		return strings.TrimPrefix(key, RawHeaderPrefix), true
	}
	return runtime.DefaultHeaderMatcher(key)
}
