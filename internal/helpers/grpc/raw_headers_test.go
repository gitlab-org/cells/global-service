package grpc_helpers_test

import (
	"testing"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/stretchr/testify/assert"

	"go.uber.org/goleak"

	grpc_helpers "gitlab.com/gitlab-org/cells/topology-service/internal/helpers/grpc"
)

func TestRawHeaderMatcher(t *testing.T) {
	tests := []struct {
		name          string
		input         string
		expectedKey   string
		expectedMatch bool
	}{
		{
			name:          "Raw header with prefix",
			input:         "rawheader-Cache-Tag",
			expectedKey:   "Cache-Tag",
			expectedMatch: true,
		},
		{
			name:          "Raw header with prefix with empty key",
			input:         "rawheader-",
			expectedKey:   "",
			expectedMatch: true,
		},
		{
			name:          "GRPC header",
			input:         "grpc-metadata-content-type",
			expectedMatch: false,
		},
		{
			name:          "Empty header",
			input:         "",
			expectedMatch: false,
		},
		{
			name:          "Only prefix without dash",
			input:         "rawheader",
			expectedMatch: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actualKey, actualMatch := grpc_helpers.RawHeaderMatcher(tt.input)

			if tt.expectedMatch {
				assert.Equal(t, tt.expectedKey, actualKey)
				assert.Equal(t, tt.expectedMatch, actualMatch)
			} else {
				expectedKey, expectedMatch := runtime.DefaultHeaderMatcher(tt.input)
				assert.Equal(t, expectedKey, actualKey)
				assert.Equal(t, expectedMatch, actualMatch)
			}
		})
	}
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
