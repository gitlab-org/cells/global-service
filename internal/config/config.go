package config

import (
	"errors"
	"fmt"

	"github.com/BurntSushi/toml"
)

var errNoCells = errors.New("no cells in config file")
var errNoServe = errors.New("no serve in config file")

type Config struct {
	Serve    []ServeConfig `toml:"serve"`
	Cells    []CellConfig  `toml:"cells"`
	Services ServiceConfig `toml:"services"`
}

type ServiceConfig struct {
	Classify ClassifyConfig `toml:"classify"`
}

func (c *Config) Validate() error {
	if len(c.Cells) == 0 {
		return errNoCells
	}
	if len(c.Serve) == 0 {
		return errNoServe
	}

	for i, s := range c.Serve {
		if err := s.Validate(); err != nil {
			return fmt.Errorf("serve[%d]: %v", i, err)
		}
	}
	for i, c := range c.Cells {
		if err := c.Validate(); err != nil {
			return fmt.Errorf("cells[%d]: %v", i, err)
		}
	}

	if err := c.Services.Validate(); err != nil {
		return fmt.Errorf("services: %v", err)
	}
	return nil
}

func (s *ServiceConfig) Validate() error {
	if err := s.Classify.Validate(); err != nil {
		return fmt.Errorf("classify: %v", err)
	}
	return nil
}

func ReadFile(file string) (*Config, error) {
	var config Config
	_, err := toml.DecodeFile(file, &config)
	if err != nil {
		return nil, err
	}
	return &config, config.Validate()
}
