package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNoDuplicateResponseHeaders(t *testing.T) {
	headers := ResponseHeaders{
		{Key: "Cache-Control", Value: "s-maxage=60"},
		{Key: "Cache-Tag", Value: "s-maxage=60"},
		{Key: "Cache-Control", Value: "s-maxage=30", Codes: []string{"OK", "InvalidArgument"}},
	}

	err := headers.Validate()
	assert.NoError(t, err)
}

func TestDuplicateDefaultResponseHeaders(t *testing.T) {
	headers := ResponseHeaders{
		{Key: "Cache-Control", Value: "s-maxage=60"},
		{Key: "Cache-Control", Value: "s-maxage=30"},
		{Key: "Cache-Tag", Value: "s-maxage=60"},
	}

	err := headers.Validate()
	assert.ErrorContains(t, err, "Duplicate key \"Cache-Control\" for no codes")
}

func TestDuplicateCodeResponseHeaders(t *testing.T) {
	headers := ResponseHeaders{
		{Key: "Cache-Control", Value: "s-maxage=60"},
		{Key: "Cache-Tag", Value: "s-maxage=60"},
		{Key: "Cache-Control", Value: "s-maxage=60", Codes: []string{"NotFound", "InvalidArgument"}},
		{Key: "Cache-Control", Value: "s-maxage=30", Codes: []string{"OK", "InvalidArgument"}},
	}

	err := headers.Validate()
	assert.ErrorContains(t, err, "Duplicate key \"Cache-Control\" for status code \"InvalidArgument\"")
}

func TestInvalidCodeResponseHeaders(t *testing.T) {
	headers := ResponseHeaders{
		{Key: "Cache-Control", Value: "s-maxage=60"},
		{Key: "Cache-Tag", Value: "s-maxage=60"},
		{Key: "Cache-Control", Value: "s-maxage=30", Codes: []string{"OK", "InvalidCode", "AnotherInvalidCode"}},
	}

	err := headers.Validate()
	assert.ErrorContains(t, err, "invalid grpc code: InvalidCode\ninvalid grpc code: AnotherInvalidCode")
}
