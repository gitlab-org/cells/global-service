package config

import (
	"errors"
	"fmt"
	"slices"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"

	grpc_helpers "gitlab.com/gitlab-org/cells/topology-service/internal/helpers/grpc"
)

var grpcCodes = map[string]codes.Code{
	"OK":                 codes.OK,
	"Canceled":           codes.Canceled,
	"Unknown":            codes.Unknown,
	"InvalidArgument":    codes.InvalidArgument,
	"DeadlineExceeded":   codes.DeadlineExceeded,
	"NotFound":           codes.NotFound,
	"AlreadyExists":      codes.AlreadyExists,
	"PermissionDenied":   codes.PermissionDenied,
	"ResourceExhausted":  codes.ResourceExhausted,
	"FailedPrecondition": codes.FailedPrecondition,
	"Aborted":            codes.Aborted,
	"OutOfRange":         codes.OutOfRange,
	"Unimplemented":      codes.Unimplemented,
	"Internal":           codes.Internal,
	"Unavailable":        codes.Unavailable,
	"DataLoss":           codes.DataLoss,
	"Unauthenticated":    codes.Unauthenticated,
}

type ResponseHeader struct {
	Key       string   `toml:"key"`
	Value     string   `toml:"value"`
	RawHeader bool     `toml:"raw_header"`
	Codes     []string `toml:"codes"`
}

func (c *ResponseHeader) Validate() error {
	if c.Key == "" {
		return errors.New("config key is empty")
	}
	if c.Value == "" {
		return errors.New("config value is empty")
	}

	var errs []error
	for _, code := range c.Codes {
		if _, ok := grpcCodes[code]; !ok {
			errs = append(errs, fmt.Errorf("invalid grpc code: %s", code))
		}
	}

	if len(errs) > 0 {
		return errors.Join(errs...)
	}

	return nil
}

type ResponseHeaders []ResponseHeader

func (r ResponseHeaders) Validate() error {
	keys := make(map[string][]string)

	const DefaultValue = "(Default)"

	for i, h := range r {
		if err := h.Validate(); err != nil {
			return fmt.Errorf("response_headers[%d]: %v", i, err)
		}

		if len(h.Codes) > 0 {
			keys[h.Key] = append(keys[h.Key], h.Codes...)
		} else {
			keys[h.Key] = append(keys[h.Key], DefaultValue)
		}
	}

	for key, values := range keys {
		slices.Sort(values)

		for i, codeCur := range values[1:] {
			codePrev := values[i]
			if codeCur == codePrev {
				if codeCur == DefaultValue {
					return fmt.Errorf("Duplicate key %q for no codes", key)
				} else {
					return fmt.Errorf("Duplicate key %q for status code %q", key, codeCur)
				}
			}
		}
	}

	return nil
}

func (r ResponseHeaders) ForCode(code codes.Code) metadata.MD {
	md := make(map[string]string)

	for _, header := range r {
		key := header.Key
		if header.RawHeader {
			key = grpc_helpers.RawHeaderPrefix + key
		}

		if len(header.Codes) == 0 {
			if _, ok := md[key]; !ok {
				md[key] = header.Value
			}
		} else {
			if slices.Contains(header.Codes, code.String()) {
				md[key] = header.Value
			}
		}
	}

	return metadata.New(md)
}
