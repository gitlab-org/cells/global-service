package config

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"net"
	"os"
)

type ServeConfig struct {
	Address                string   `toml:"address"`
	CertificateFile        string   `toml:"certificate_file"`
	KeyFile                string   `toml:"key_file"`
	ClientCertificateFiles []string `toml:"client_certificate_files"`
	Features               []string `toml:"features"`
}

func (c *ServeConfig) Validate() error {
	if c.Address == "" {
		return errors.New("address is empty")
	}
	if len(c.Features) == 0 {
		return errors.New("no features specified")
	}
	if c.Secure() {
		if c.CertificateFile == "" {
			return errors.New("certificate_file is empty")
		}
		if c.KeyFile == "" {
			return errors.New("key_file is empty")
		}
	}
	return nil
}

// Checks if pattern in config does match feature
func (c *ServeConfig) HasFeature(name ServeFeature) bool {
	for _, f := range c.Features {
		if name.Matches(f) {
			return true
		}
	}
	return false
}

// Any use of certificates triggers TLS
func (c *ServeConfig) Secure() bool {
	return c.CertificateFile != "" || c.KeyFile != "" || len(c.ClientCertificateFiles) != 0
}

// Mutual TLS requires client certificates
func (c *ServeConfig) SecureAndAuthenticated() bool {
	return len(c.ClientCertificateFiles) != 0
}

func (c *ServeConfig) Mode() string {
	if c.SecureAndAuthenticated() {
		return "mTLS"
	} else if c.Secure() {
		return "TLS"
	} else {
		return "Insecure"
	}
}

func (c *ServeConfig) TlsConfig() (*tls.Config, error) {
	if !c.Secure() {
		return nil, nil
	}

	if c.CertificateFile == "" || c.KeyFile == "" {
		return nil, fmt.Errorf("certificate files not specified")
	}

	certificate, err := tls.LoadX509KeyPair(c.CertificateFile, c.KeyFile)
	if err != nil {
		return nil, fmt.Errorf("failed to load server certification: %w", err)
	}

	if c.SecureAndAuthenticated() {
		tlsConfig := &tls.Config{
			ClientAuth:   tls.RequireAndVerifyClientCert,
			Certificates: []tls.Certificate{certificate},
			ClientCAs:    x509.NewCertPool(),
		}

		for _, clientCertificateFile := range c.ClientCertificateFiles {
			clientCertificate, err := os.ReadFile(clientCertificateFile)

			if err != nil {
				return nil, fmt.Errorf("failed to read client certificate: %v", err)
			}
			if !tlsConfig.ClientCAs.AppendCertsFromPEM(clientCertificate) {
				return nil, fmt.Errorf("unable to append client certificate: %v", clientCertificateFile)
			}
		}
		return tlsConfig, nil
	}

	tlsConfig := &tls.Config{
		ClientAuth:   tls.NoClientCert,
		Certificates: []tls.Certificate{certificate},
	}
	return tlsConfig, nil
}

func (c *ServeConfig) Socket() (net.Listener, error) {
	socket, err := net.Listen("tcp", c.Address)
	if err != nil {
		return nil, err
	}
	return socket, nil
}
