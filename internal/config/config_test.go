package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/goleak"
)

func TestConfig(t *testing.T) {
	cellConfig := CellConfig{
		Id:            1,
		Address:       "cell-1.gitlab.com",
		SessionPrefix: "cell1",
		SequenceRange: [2]int64{4398046511104, 8796093022207},
	}

	serveConfig := ServeConfig{
		Address:  ":8090",
		Features: []string{"*_rest"},
	}

	tests := map[string]struct {
		Config   Config
		Expected string
	}{
		"valid": {
			Config{
				Cells: []CellConfig{cellConfig},
				Serve: []ServeConfig{serveConfig},
			},
			"",
		},
		"cell missing id": {
			Config{
				Cells: []CellConfig{
					{
						Address: "cell-1.gitlab.com",
					},
				},
				Serve: []ServeConfig{serveConfig},
			},
			"cells[0]: cell ID is less than 1",
		},
		"cell missing address": {
			Config{
				Cells: []CellConfig{
					{
						Id: 1,
					},
				},
				Serve: []ServeConfig{serveConfig},
			},
			"cells[0]: cell address is empty",
		},
		"cell missing session prefix": {
			Config{
				Cells: []CellConfig{
					{
						Id:      1,
						Address: "cell-1.gitlab.com",
					},
				},
				Serve: []ServeConfig{serveConfig},
			},
			"cells[0]: cell session prefix is empty",
		},
		"serve missing address": {
			Config{
				Cells: []CellConfig{cellConfig},
				Serve: []ServeConfig{
					{
						Features: []string{"*_rest"},
					},
				},
			},
			"serve[0]: address is empty",
		},
		"serve missing features": {
			Config{
				Cells: []CellConfig{cellConfig},
				Serve: []ServeConfig{
					{
						Address: ":8090",
					},
				},
			},
			"serve[0]: no features specified",
		},
		"serve missing certificate file": {
			Config{
				Cells: []CellConfig{cellConfig},
				Serve: []ServeConfig{
					{
						Address:                ":8090",
						Features:               []string{"*_rest"},
						ClientCertificateFiles: []string{"client-cert"},
					},
				},
			},
			"serve[0]: certificate_file is empty",
		},
		"serve missing certificate key": {
			Config{
				Cells: []CellConfig{cellConfig},
				Serve: []ServeConfig{
					{
						Address:         ":8090",
						Features:        []string{"*_rest"},
						CertificateFile: "server-cert",
					},
				},
			},
			"serve[0]: key_file is empty",
		},
		"no serve": {
			Config{
				Cells: []CellConfig{cellConfig},
			},
			errNoServe.Error(),
		},
		"empty": {
			Config{},
			errNoCells.Error(),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			err := test.Config.Validate()
			if test.Expected != "" {
				assert.EqualError(t, err, test.Expected)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
