package config

import (
	"errors"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
)

type CellConfig struct {
	Id            int64    `toml:"id"`
	Address       string   `toml:"address"`
	SessionPrefix string   `toml:"session_prefix"`
	SequenceRange [2]int64 `toml:"sequence_range"`
}

func (c *CellConfig) Validate() error {
	if c.Id < 1 {
		return errors.New("cell ID is less than 1")
	}
	if c.Address == "" {
		return errors.New("cell address is empty")
	}
	if c.SessionPrefix == "" {
		return errors.New("cell session prefix is empty")
	}
	if c.SequenceRange[0] >= c.SequenceRange[1] {
		return errors.New("cell sequence min limit should be less than max limit")
	}
	return nil
}

func (c *CellConfig) Proto() *proto.CellInfo {
	return &proto.CellInfo{
		Id:            c.Id,
		Address:       c.Address,
		SessionPrefix: c.SessionPrefix,
		SequenceRange: &proto.SequenceRange{
			Minval: c.SequenceRange[0],
			Maxval: c.SequenceRange[1],
		},
	}
}
