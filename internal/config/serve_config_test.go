package config

import (
	"crypto/tls"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const caFile = "../../tmp/certs/ca-cert.pem"
const certFile = "../../tmp/certs/server-cert.pem"
const keyFile = "../../tmp/certs/server-key.pem"

func TestServeConfig_Socket(t *testing.T) {
	s := &ServeConfig{}
	socket, err := s.Socket()
	require.NoError(t, err)
	require.NotNil(t, socket)
}

func TestServeConfig_TlsConfig(t *testing.T) {
	tests := map[string]struct {
		Serve        ServeConfig
		ExpectedMode string
		ExpectedErr  string
	}{
		"valid insecure": {
			ServeConfig{},
			"Insecure",
			"",
		},
		"valid TLS": {
			ServeConfig{CertificateFile: certFile, KeyFile: keyFile},
			"TLS",
			"",
		},
		"valid mTLS": {
			ServeConfig{CertificateFile: certFile, KeyFile: keyFile, ClientCertificateFiles: []string{caFile}},
			"mTLS",
			"",
		},

		"error when only client certificates": {
			ServeConfig{ClientCertificateFiles: []string{caFile}},
			"mTLS",
			"certificate files not specified",
		},
		"error when only private key": {
			ServeConfig{KeyFile: keyFile},
			"TLS",
			"certificate files not specified",
		},
		"error when only certificate": {
			ServeConfig{CertificateFile: certFile},
			"TLS",
			"certificate files not specified",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			tlsConfig, err := test.Serve.TlsConfig()
			assert.Equal(t, test.ExpectedMode, test.Serve.Mode())

			if test.ExpectedErr != "" {
				require.EqualError(t, err, test.ExpectedErr)
				require.Nil(t, tlsConfig)
				return
			}

			require.NoError(t, err)

			switch test.ExpectedMode {
			case "Insecure":
				require.Nil(t, tlsConfig)

			case "TLS":
				require.NotNil(t, tlsConfig)
				require.Equal(t, tls.NoClientCert, tlsConfig.ClientAuth)

			case "mTLS":
				require.NotNil(t, tlsConfig)
				require.Equal(t, tls.RequireAndVerifyClientCert, tlsConfig.ClientAuth)

			default:
				require.Nil(t, tlsConfig)
			}
		})
	}
}

func TestServeConfig_Features(t *testing.T) {
	tests := map[string]struct {
		Features []string
		Check    ServeFeature
		Expected bool
	}{
		"positive exact match": {
			[]string{"health_grpc"},
			ServeFeature_HealthService_gRPC,
			true,
		},
		"positive wildcard match": {
			[]string{"*_grpc"},
			ServeFeature_HealthService_gRPC,
			true,
		},
		"negative exact match": {
			[]string{"health_grpc"},
			ServeFeature_HealthService_REST,
			false,
		},
		"negative wildcard match": {
			[]string{"*_grpc"},
			ServeFeature_HealthService_REST,
			false,
		},
		"invalid pattern returns false": {
			[]string{"[_grpc"},
			ServeFeature_HealthService_REST,
			false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			s := &ServeConfig{Features: test.Features}
			require.Equal(t, test.Expected, s.HasFeature(test.Check))
		})
	}
}
