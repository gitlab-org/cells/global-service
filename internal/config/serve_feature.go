package config

import (
	"path/filepath"
)

type ServeFeature string

const (
	ServeFeature_HealthService_gRPC   ServeFeature = "health_grpc"
	ServeFeature_HealthService_REST                = "health_rest"
	ServeFeature_ClassifyService_gRPC              = "classify_grpc"
	ServeFeature_ClassifyService_REST              = "classify_rest"
	ServeFeature_ClaimService_gRPC                 = "claim_grpc"
	ServeFeature_CellService_gRPC                  = "cell_grpc"
)

var AllFeatures = []ServeFeature{
	ServeFeature_HealthService_gRPC,
	ServeFeature_HealthService_REST,
	ServeFeature_ClassifyService_gRPC,
	ServeFeature_ClassifyService_REST,
	ServeFeature_ClaimService_gRPC,
	ServeFeature_CellService_gRPC,
}

func (f ServeFeature) Matches(pattern string) bool {
	if m, err := filepath.Match(pattern, string(f)); m && err == nil {
		return true
	}
	return false
}

func (f ServeFeature) IsGRPC() bool {
	return f.Matches("*_grpc")
}

func (f ServeFeature) IsREST() bool {
	return f.Matches("*_rest")
}
