package config

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

type ClassifyConfig struct {
	ResponseHeaders ResponseHeaders `toml:"response_headers"`
}

func (c *ClassifyConfig) Validate() error {
	if err := c.ResponseHeaders.Validate(); err != nil {
		return err
	}

	return nil
}

func (c *ClassifyConfig) ResponseHeadersForCode(code codes.Code) metadata.MD {
	return c.ResponseHeaders.ForCode(code)
}
