package health_service

import (
	"context"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
)

type healthServiceServer struct {
	proto.UnsafeHealthServiceServer
}

func (s *healthServiceServer) ReadinessProbe(context.Context, *proto.ReadinessProbeRequest) (*proto.ReadinessProbeResponse, error) {
	return &proto.ReadinessProbeResponse{}, nil
}

func (s *healthServiceServer) LivenessProbe(context.Context, *proto.LivenessProbeRequest) (*proto.LivenessProbeResponse, error) {
	return &proto.LivenessProbeResponse{}, nil
}

// Generate compilation error on added RPC
func (s *healthServiceServer) mustEmbedUnimplementedHealthServiceServer() {
}

// New returns a new HealthServiceServer
func New(_ *config.Config) proto.HealthServiceServer {
	return &healthServiceServer{}
}
