package classify_service

import (
	"context"
	"fmt"
	"strconv"

	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
	"gitlab.com/gitlab-org/labkit/log"
)

func (s *classifyServiceServer) setResponseHeaders(ctx context.Context, code codes.Code) {
	md := s.GetConfig().Services.Classify.ResponseHeadersForCode(code)
	grpc.SetHeader(ctx, md)
}

func (s *classifyServiceServer) proxyToCell(_ context.Context, cell *config.CellConfig) *proto.ClassifyResponse {
	return &proto.ClassifyResponse{
		Action: proto.ClassifyAction_PROXY,
		Proxy: &proto.ProxyInfo{
			Address: cell.Address,
		},
	}
}

func (s *classifyServiceServer) classifyFirstCell(ctx context.Context) (*proto.ClassifyResponse, error) {
	return s.proxyToCell(ctx, &s.GetConfig().Cells[0]), nil
}

func (s *classifyServiceServer) classifySessionPrefix(ctx context.Context, req *proto.ClassifyRequest) (*proto.ClassifyResponse, error) {
	for _, cell := range s.GetConfig().Cells {
		if cell.SessionPrefix == req.Value {
			return s.proxyToCell(ctx, &cell), nil
		}
	}

	return nil, status.Errorf(codes.NotFound, "cell not found: %q", req.Value)
}

func (s *classifyServiceServer) classifyCellId(ctx context.Context, req *proto.ClassifyRequest) (*proto.ClassifyResponse, error) {
	valueStr := req.GetValue()
	cellId, err := strconv.ParseInt(valueStr, 10, 64)
	if err != nil {
		st := status.New(codes.InvalidArgument, fmt.Sprintf("invalid value: %q", valueStr))
		ds, withDetailsErr := st.WithDetails(&errdetails.BadRequest_FieldViolation{
			Field:       "value",
			Description: fmt.Sprintf("value must be a valid integer string, when `classify_type` is set to CELL_ID: %v", err),
		})
		if withDetailsErr != nil {
			return nil, st.Err()
		}
		return nil, ds.Err()
	}

	for _, cell := range s.GetConfig().Cells {
		if cell.Id == cellId {
			return s.proxyToCell(ctx, &cell), nil
		}
	}

	return nil, status.Errorf(codes.NotFound, "cell not found: %q", valueStr)
}

func (s *classifyServiceServer) Classify(ctx context.Context, req *proto.ClassifyRequest) (res *proto.ClassifyResponse, err error) {
	log.WithFields(log.Fields{
		"type":  req.Type,
		"value": req.Value,
	}).Infof("Classify")

	defer func() {
		s.setResponseHeaders(ctx, status.Code(err))
	}()

	switch req.Type {
	case proto.ClassifyType_FIRST_CELL:
		return s.classifyFirstCell(ctx)
	case proto.ClassifyType_SESSION_PREFIX:
		return s.classifySessionPrefix(ctx, req)
	case proto.ClassifyType_CELL_ID:
		return s.classifyCellId(ctx, req)
	default:
		return nil, status.Errorf(codes.InvalidArgument, "invalid type: %q", req.Type)
	}
}
