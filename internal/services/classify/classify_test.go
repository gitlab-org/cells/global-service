package classify_service_test

import (
	"bytes"
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	test_helpers "gitlab.com/gitlab-org/cells/topology-service/internal/helpers/test"
	classify_service "gitlab.com/gitlab-org/cells/topology-service/internal/services/classify"
	"gitlab.com/gitlab-org/labkit/log"

	"go.uber.org/goleak"
)

func TestClassify(t *testing.T) {
	cfg := test_helpers.TestConfig(t)
	srv := classify_service.New(cfg)

	tests := []struct {
		name              string
		request           *proto.ClassifyRequest
		expected          *proto.ClassifyResponse
		expectedErr       string
		expectedHeaders   map[string]string
		expectedLogRegExp string
	}{
		{
			name:        "No Type Passed",
			request:     &proto.ClassifyRequest{},
			expectedErr: "rpc error: code = InvalidArgument desc = invalid type: \"UNSPECIFIED\"",
			expectedHeaders: map[string]string{
				"rawheader-cache-control": "s-maxage=5",
				"rawheader-cache-tag":     "gprd_topology_service_gitlab_com",
			},
			expectedLogRegExp: `^$`,
		},
		{
			name:    "Type is 'FirstCell'",
			request: &proto.ClassifyRequest{Type: proto.ClassifyType_FIRST_CELL},
			expected: &proto.ClassifyResponse{
				Action: proto.ClassifyAction_PROXY,
				Proxy: &proto.ProxyInfo{
					Address: "cell-1.example.com",
				},
			},
			expectedHeaders: map[string]string{
				"rawheader-cache-control": "s-maxage=60",
				"rawheader-cache-tag":     "gprd_topology_service_gitlab_com",
			},
			expectedLogRegExp: `level=info msg=Classify type=FIRST_CELL value=\n$`,
		},
		{
			name:    "Type is 'SessionPrefix', with a valid cell's prefix",
			request: &proto.ClassifyRequest{Type: proto.ClassifyType_SESSION_PREFIX, Value: "cell-1"},
			expected: &proto.ClassifyResponse{
				Action: proto.ClassifyAction_PROXY,
				Proxy: &proto.ProxyInfo{
					Address: "cell-1.example.com",
				},
			},
			expectedHeaders: map[string]string{
				"rawheader-cache-control": "s-maxage=60",
				"rawheader-cache-tag":     "gprd_topology_service_gitlab_com",
			},
			expectedLogRegExp: `level=info msg=Classify type=SESSION_PREFIX value=cell-1\n$`,
		},
		{
			name:        "Type is 'SessionPrefix', with an invalid prefix",
			request:     &proto.ClassifyRequest{Type: proto.ClassifyType_SESSION_PREFIX, Value: "wrong-prefix"},
			expectedErr: "rpc error: code = NotFound desc = cell not found: \"wrong-prefix\"",
			expectedHeaders: map[string]string{
				"rawheader-cache-control": "s-maxage=5",
				"rawheader-cache-tag":     "gprd_topology_service_gitlab_com",
			},
			expectedLogRegExp: `level=info msg=Classify type=SESSION_PREFIX value=wrong-prefix\n$`,
		},
		{
			name:    "Type is 'CELL_ID', with a valid cell's ID",
			request: &proto.ClassifyRequest{Type: proto.ClassifyType_CELL_ID, Value: "100"},
			expected: &proto.ClassifyResponse{
				Action: proto.ClassifyAction_PROXY,
				Proxy: &proto.ProxyInfo{
					Address: "cell-1.example.com",
				},
			},
			expectedHeaders: map[string]string{
				"rawheader-cache-control": "s-maxage=60",
				"rawheader-cache-tag":     "gprd_topology_service_gitlab_com",
			},
			expectedLogRegExp: `level=info msg=Classify type=CELL_ID value=100\n$`,
		},
		{
			name:        "Type is 'CELL_ID', with an non-existing cell's ID",
			request:     &proto.ClassifyRequest{Type: proto.ClassifyType_CELL_ID, Value: "420"},
			expectedErr: "rpc error: code = NotFound desc = cell not found: \"420\"",
			expectedHeaders: map[string]string{
				"rawheader-cache-control": "s-maxage=5",
				"rawheader-cache-tag":     "gprd_topology_service_gitlab_com",
			},
			expectedLogRegExp: `level=info msg=Classify type=CELL_ID value=420\n$`,
		},
		{
			name:        "Type is 'CELL_ID', with an invalid cell's ID",
			request:     &proto.ClassifyRequest{Type: proto.ClassifyType_CELL_ID, Value: "wrong-id"},
			expectedErr: "rpc error: code = InvalidArgument desc = invalid value: \"wrong-id\"",
			expectedHeaders: map[string]string{
				"rawheader-cache-control": "s-maxage=5",
				"rawheader-cache-tag":     "gprd_topology_service_gitlab_com",
			},
			expectedLogRegExp: `level=info msg=Classify type=CELL_ID value=wrong-id\n$`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			headerMD := metadata.MD{}
			mockStream := &mockServerTransportStream{header: &headerMD}
			ctx := metadata.NewIncomingContext(context.Background(), metadata.MD{})
			ctx = grpc.NewContextWithServerTransportStream(ctx, mockStream)
			buf := &bytes.Buffer{}
			closer, err := log.Initialize(log.WithWriter(buf))
			require.NoError(t, err)
			defer closer.Close()

			res, err := srv.Classify(ctx, tt.request)

			if tt.expectedErr != "" {
				assert.ErrorContains(t, err, tt.expectedErr)
				assert.Nil(t, res)

				actualHeaders := headerMD
				assert.Equal(t, metadata.New(tt.expectedHeaders), actualHeaders)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, tt.expected, res)

			actualHeaders := headerMD
			assert.Equal(t, metadata.New(tt.expectedHeaders), actualHeaders)

			require.Regexp(t, tt.expectedLogRegExp, buf.String())
		})
	}
}

type mockServerTransportStream struct {
	header *metadata.MD
	grpc.ServerTransportStream
}

func (m *mockServerTransportStream) SetHeader(md metadata.MD) error {
	*m.header = md
	return nil
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
