package classify_service

import (
	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
	common_service "gitlab.com/gitlab-org/cells/topology-service/internal/services/common"
)

type classifyServiceServer struct {
	common_service.CommonServiceServer
	proto.UnsafeClassifyServiceServer
}

// Generate compilation error on added RPC
func (s *classifyServiceServer) mustEmbedUnimplementedClassifyServiceServer() {
}

func New(cfg *config.Config) proto.ClassifyServiceServer {
	return &classifyServiceServer{
		CommonServiceServer: common_service.New(cfg),
	}
}
