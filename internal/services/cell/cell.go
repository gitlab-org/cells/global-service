package cell_service

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/labkit/log"
)

func (s *cellServiceServer) GetCell(_ context.Context, req *proto.GetCellRequest) (*proto.GetCellResponse, error) {
	cellId := req.GetCellId()

	log.WithFields(log.Fields{
		"id": cellId,
	}).Infof("GetCell")

	res := &proto.GetCellResponse{}

	for _, cell := range s.GetConfig().Cells {
		if cell.Id == cellId {
			res.CellInfo = cell.Proto()
			return res, nil
		}
	}

	return nil, status.Errorf(codes.NotFound, "cell not found: %d", cellId)
}
