package cell_service

import (
	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
	common_service "gitlab.com/gitlab-org/cells/topology-service/internal/services/common"
)

type cellServiceServer struct {
	common_service.CommonServiceServer
	proto.UnsafeCellServiceServer
}

// Generate compilation error on added RPC
func (s *cellServiceServer) mustEmbedUnimplementedCellServiceServer() {
}

func New(cfg *config.Config) proto.CellServiceServer {
	return &cellServiceServer{
		CommonServiceServer: common_service.New(cfg),
	}
}
