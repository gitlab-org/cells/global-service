package claim_service_test

import (
	"bytes"
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.uber.org/goleak"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	test_helpers "gitlab.com/gitlab-org/cells/topology-service/internal/helpers/test"
	claim_service "gitlab.com/gitlab-org/cells/topology-service/internal/services/claim"
	"gitlab.com/gitlab-org/labkit/log"
)

func TestValidClaim(t *testing.T) {
	cfg := test_helpers.TestConfig(t)
	srv := claim_service.New(cfg)
	buf := &bytes.Buffer{}
	closer, err := log.Initialize(log.WithWriter(buf))
	require.NoError(t, err)
	defer closer.Close()

	claimDetails := &proto.ClaimDetails{
		Claim:  &proto.ClaimRecord{Bucket: proto.ClaimRecord_ROUTES, Value: "gitlab-org/gitlab"},
		Parent: &proto.ParentRecord{Model: proto.ParentRecord_PROJECT, Id: 1},
		Owner:  &proto.OwnerRecord{Table: proto.OwnerRecord_ROUTES, Id: 2},
	}

	request := &proto.CreateClaimRequest{Details: claimDetails}
	res, err := srv.CreateClaim(context.Background(), request)
	assert.Greater(t, res.Claim.Id, int64(0))
	assert.NotNil(t, res.Claim.CellInfo)
	assert.Nil(t, err)
	require.Regexp(t, `level=info msg=CreateClaim details="claim:{bucket:ROUTES *value:\\"gitlab-org/gitlab\\"} *parent:{model:PROJECT *id:1} *owner:{table:ROUTES *id:2}"\n$`, buf.String())
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
