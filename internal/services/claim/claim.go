package claim_service

import (
	"context"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/labkit/log"
)

func (s *claimServiceServer) CreateClaim(ctx context.Context, req *proto.CreateClaimRequest) (*proto.CreateClaimResponse, error) {
	log.WithFields(log.Fields{
		"details": req.Details,
	}).Infof("CreateClaim")

	claimInfo, err := s.claimStore.Create(ctx, req.Details)
	if err != nil {
		return nil, err
	}

	var response = &proto.CreateClaimResponse{Claim: claimInfo}

	return response, nil
}
