package claim_service

import (
	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
	common_service "gitlab.com/gitlab-org/cells/topology-service/internal/services/common"
	"gitlab.com/gitlab-org/cells/topology-service/internal/store"
	null_store "gitlab.com/gitlab-org/cells/topology-service/internal/store/null"
)

type claimServiceServer struct {
	common_service.CommonServiceServer
	proto.UnsafeClaimServiceServer
	claimStore store.ClaimStore
}

// Generate compilation error on added RPC
func (s *claimServiceServer) mustEmbedUnimplementedClaimServiceServer() {
}

func New(cfg *config.Config) proto.ClaimServiceServer {
	return &claimServiceServer{
		CommonServiceServer: common_service.New(cfg),
		claimStore:          null_store.NewClaimStore(cfg), // TODO: To be configurable
	}
}
