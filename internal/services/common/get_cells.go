package common_service

import (
	"context"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/labkit/log"
)

func (s *commonService) GetCells(_ context.Context, _ *proto.GetCellsRequest) (*proto.GetCellsResponse, error) {
	log.WithFields(log.Fields{}).Infof("GetCells")

	res := &proto.GetCellsResponse{}
	for _, cell := range s.cfg.Cells {
		res.Cells = append(res.Cells, cell.Proto())
	}
	return res, nil
}
