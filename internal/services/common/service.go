package common_service

import (
	"context"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
)

type commonService struct {
	cfg *config.Config
}

type CommonServiceServer interface {
	GetConfig() *config.Config
	GetCells(context.Context, *proto.GetCellsRequest) (*proto.GetCellsResponse, error)
}

func (s *commonService) GetConfig() *config.Config {
	return s.cfg
}

func New(cfg *config.Config) CommonServiceServer {
	return &commonService{
		cfg: cfg,
	}
}
