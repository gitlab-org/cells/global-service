package common_service_test

import (
	"bytes"
	"context"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	test_helpers "gitlab.com/gitlab-org/cells/topology-service/internal/helpers/test"
	common_service "gitlab.com/gitlab-org/cells/topology-service/internal/services/common"
	"gitlab.com/gitlab-org/labkit/log"

	"go.uber.org/goleak"
)

func TestGetCells(t *testing.T) {
	cfg := test_helpers.TestConfig(t)
	srv := common_service.New(cfg)
	buf := &bytes.Buffer{}
	closer, err := log.Initialize(log.WithWriter(buf))
	require.NoError(t, err)
	defer closer.Close()

	res, err := srv.GetCells(context.Background(), &proto.GetCellsRequest{})
	require.NoError(t, err)
	require.NotEmpty(t, res.Cells)
	require.Regexp(t, `level=info msg=GetCells\n$`, buf.String())
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
