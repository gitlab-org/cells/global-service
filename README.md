# Topology Service

The purpose of Topology Service is to provide essential features for Cells to operate.
The Topology Service does implement a limited set of functions and serve as an authoritative entity within the Cluster.
There’s only a single Topology Service in the Cluster, that can be deployed in many regions.

See [Topology Service Blueprint](https://docs.gitlab.com/ee/architecture/blueprints/cells/topology_service.html).

## Documentation

- [The `config.toml` file](docs/config.md)
- [The development environment](docs/development.md)
- [HTTP endpoint documentations](docs/api)

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).
