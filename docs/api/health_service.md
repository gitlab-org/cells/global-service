## Health Service

### Liveness Probe

Used to check if application is alive or it should be restarted.

```plaintext
GET /v1/health/liveness
```

If successful, returns [`200`].

Example request:

```shell
curl -X GET http://localhost:9096/v1/health/liveness
```

Example response:

```json
{}
```

### Readiness Probe

Used to check if application is ready to accept requests.

```plaintext
GET /v1/health/readiness
```

If successful, returns [`200`].

Example request:

```shell
curl -X GET http://localhost:9096/v1/health/readiness
```

Example response:

```json
{}
```
