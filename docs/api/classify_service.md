## Classify Service

### Classify

Given a type and value, returns an action and proxy.

```plaintext
POST /api/v1/classify
```

Supported attributes:

| Attribute                | Type     | Required | Description           |
|--------------------------|----------|----------|-----------------------|
| `type`                   | string   | Yes      | Classify type         |
| `value`                  | string   | Yes      | Classify value        |

If successful, returns [`200`] and the following
response attributes:

| Attribute                | Type     | Description           |
|--------------------------|----------|-----------------------|
| `action`                 | string   | Classify action       |
| `proxy`                  | ProxyInfo| Proxy                 |

Example request:

```shell
curl -X POST \
     -H "Content-Type: application/json" \
     -d '{"type":"FIRST_CELL","value":"99"}' \
     http://localhost:9096/v1/classify
```

Example response:

```json
{
   "action" : "PROXY",
   "proxy" : {
      "address" : "127.0.0.1:3333"
   }
}
```

| HTTP status| Description                     | Retryable? |
|------------|---------------------------------|------------|
| `200`      | Successful                      | N/A        |
| `400`      | User provided invalid arguments | No         |
| `404`      | Cell could not be found         | No         |
| `503`      | Service is unavailable          | Yes        |
| `504`      | Deadline was exceeded           | Yes        |
