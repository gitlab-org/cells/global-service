# The `config.toml` file

NOTE:
Whenever there is a breaking change in the config, it has to be updated in the
[deployer](https://gitlab.com/gitlab-com/gl-infra/cells/topology-service-deployer) and GDK's `config.toml`
[template file](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/support/templates/gitlab-topology-service/config.toml.erb). 

| Section   | Required | Description                                                 |
|-----------|----------|-------------------------------------------------------------|
| `cells`   | Yes      | Defines the cells that the server will connect to.          |
| `serve`   | Yes      | Specifies the server configuration for handling requests.  |

## `[[cells]]` Section

The `[[cells]]` section is required and must have at least one item to make the configuration valid.

| Field            | Required | Type   | Description                                                                              |
|------------------|----------|--------|------------------------------------------------------------------------------------------|
| `id`             | Yes      | int    | The unique identifier for the cell.                                                      |
| `name`           | Yes      | string | The name of the cell.                                                                    |
| `address`        | Yes      | string | The address of the cell.                                                                 |
| `session_prefix` | Yes      | string | The session prefix of the cell that will be used by HTTP router.                         |
| `sequence_range` | Yes      | Array  | Min and Max value for the database sequences, given range should be unique across cells. |

### Example

```toml
[[cells]]
id = 100
name = "cell-1"
address = "my.cell-1.example.com"
session_prefix = "cell-1"
sequence_range = [0, 4398046511104]
```

## `[[serve]]` Section

The `[[serve]]` section is required and must have at least one item to make the configuration valid.

| Field                     | Required | Type     | Description                                                                 |
|---------------------------|----------|----------|-----------------------------------------------------------------------------|
| `address`                 | Yes      | string   | The address on which the server will listen for requests.                   |
| `certificate_file`        | No       | string   | The path to the server's certificate file for TLS.                          |
| `key_file`                | No       | string   | The path to the server's private key file for TLS.                          |
| `client_certificate_files`| No       | []string | The paths to the client certificate files for mTLS.                         |
| `features`                | Yes      | []string | The list of features to enable for the server.                              |

### Example

```toml
[[serve]]
address = ":9095"
certificate_file = "tmp/certs/server-cert.pem"
key_file = "tmp/certs/server-key.pem"
client_certificate_files = ["tmp/certs/client-cert.pem"]
features = ["*_grpc"]
```

### Features

The `features` field in the `[[serve]]` section specifies the services and protocols to enable for the server.
The available features are:

| Service         | Config Option              | Protocol | Requires Client Certificates |
|-----------------|----------------------------|----------|------------------------------|
| HealthService   | `health_grpc`, `health_rest`| gRPC, REST | No                           |
| ClassifyService | `classify_grpc`, `classify_rest`| gRPC, REST | No                           |
| ClaimService    | `claim_grpc`               | gRPC     | Yes                          |
| SequenceService | `sequence_grpc`            | gRPC     | Yes                          |

You can specify either gRPC or REST features for each `[[serve]]` section, but not both at the same time. To enable all gRPC or all REST features, you can use the wildcard `*_grpc` or `*_rest`, respectively.

### TLS and mTLS

The server can be configured with TLS or mTLS:

- For TLS, only the `certificate_file` and `key_file` fields are required.
- For mTLS, the `client_certificate_files` field must also be specified, along with the `certificate_file` and `key_file`.
- Multiple client certificate CA files can be specified in the `client_certificate_files` field.
- CRLs (Certificate Revocation Lists) are not supported.

---

> Disclaimer: This documentation was generated with the assistance of Anthropic.

## `[[services.classify.response_headers]]` Section

The `[[services.classify.response_headers]]` section is optional, and if provided it must have valid configuration. You can specify multiple headers.

| Field        | Required | Type            | Description                                                                                                     |
|--------------|----------|-----------------|-----------------------------------------------------------------------------------------------------------------|
| `key`        | Yes      | `string`        | The header's key.                                                                                               |
| `value`      | Yes      | `string`        | The header's value.                                                                                             |
| `raw_header` | No       | `boolean`       | Regular headers are prefixed with `Grpc-Metadata-`, raw headers do not have prefix when sent in HTTP response.  |
| `codes`      | No       | `Array[String]` | List of [response codes](https://pkg.go.dev/google.golang.org/grpc/codes) to which the headers will be applied. |

### Example

```toml
[[services.classify.response_headers]]
key = "Cache-Control"
value = "s-maxage=1"
raw_header = true

[[services.classify.response_headers]]
key = "Cache-Control"
value = "s-maxage=10"
raw_header = true
codes = ["OK"]

[[services.classify.response_headers]]
key = "Cache-Tag"
value = "gprd_topology_service_gitlab_com"
raw_header = true
codes = ["NotFound", "InvalidArgument"]
```
