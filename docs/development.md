# The development environment

## Setting up your development environment

### Installing Dependencies

   ```bash
   make deps
   asdf plugin-add protoc https://github.com/paxosglobal/asdf-protoc.git
   asdf install
   ```

#### macOS-specific instructions

In addition to the `make deps` command, you'll need to install the `grpc` Homebrew package:

   ```bash
   brew install grpc
   ```

## Running your development environment

   ```bash
   make fmt
   make build
   make test
   ```

## Update generated files

   ```bash
   make generate
   ```

## Running the linter locally

In case you don't have golangci-lint yet:
```bash 
brew install golangci-lint
```
To run the linter:
```bash
golangci-lint run
```
Use `-v` for verbose output.


## Updating your development environment

To update, `git pull` on the `main` branch:

```sh
git checkout main
git pull
```

## Starting the Topology Service locally

```
go run . serve
```

## Updating Topology Service Gem on GitLab-org/GitLab

To update the Topology Service Gem in the Rails monolith application, please
refer to:

<https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/cells/topology_service.md>

## Send a request to a local running instance with grpcurl

As an example, if it's running on port 9095, we can run this command:

```
$ grpcurl -proto proto/claim_service.proto \
          -plaintext \
          -d '{}' \
          127.0.0.1:9095 \
          gitlab.cells.topology_service.ClaimService/GetCells
```
