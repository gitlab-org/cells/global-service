# Deployment

The deployment configuration sits in the [topology-service-deployer](https://gitlab.com/gitlab-com/gl-infra/cells/topology-service-deployer) project. 
See details on service configuration [here](./config.md). If changes are required to the configuration or for scaling they happen in the 
[topology-service-deployer](https://gitlab.com/gitlab-com/gl-infra/cells/topology-service-deployer) project, a merge in that project will also trigger a 
deployment.

The deployment happens on every push to main in [toplology-service](https://gitlab.com/gitlab-org/cells/topology-service) project by triggering a pipeline
run in the [topology-service-deployer](https://gitlab.com/gitlab-com/gl-infra/cells/topology-service-deployer) project. When the deployment completes a commit
is made to the [topology-service-deployer](https://gitlab.com/gitlab-com/gl-infra/cells/topology-service-deployer) project to update the latest version

A dry-run deployment happens every time a merge train is triggered in the [toplology-service](https://gitlab.com/gitlab-org/cells/topology-service) project.

See the diagram below:
![Project Diagram](./images/topology-service-deployment.png)

[source](https://excalidraw.com/#json=w2gs_8qM78HE9gpgF8vdp,nEjPrcejH5IWUrPQ9dMRHw)
