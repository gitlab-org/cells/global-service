#!/bin/sh

set -e 

echo "$CONFIG_TOML" > /tmp/config.toml

cat /tmp/config.toml

/app/topology-service serve --config /tmp/config.toml
