export PATH := $(shell go env GOPATH)/bin:$(PATH)
export CONFIG_FILE ?= $(CURDIR)/configs/config-simple.toml

all: build

.PHONY: certs
certs:
	[ -e tmp/certs/done ] || scripts/generate-certs.bash

.PHONY: deps
deps: certs config.toml
	go install google.golang.org/protobuf/cmd/protoc-gen-go
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway
	go install github.com/yoheimuta/protolint/cmd/protolint
	go install github.com/bufbuild/buf/cmd/buf
	cd clients/ruby && bundle install

config.toml:
	cp configs/config-simple.toml config.toml

.PHONY: generate
generate:
	buf generate

.PHONY: fmt
fmt:
	go mod tidy
	go fmt ./...
	protolint lint proto/*.proto
	cd clients/ruby && bundle exec rubocop

.PHONY: build
build:
	CGO_ENABLED=0 go build .

.PHONY: test-go
test-go: build
	go test -v -race ./...

.PHONY: test-ruby
test-ruby: build
	cd clients/ruby && bundle exec rspec

.PHONY: test
test: test-go test-ruby
