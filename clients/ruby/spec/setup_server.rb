# frozen_string_literal: true

require "fileutils"
require "logger"
require "net/http"

module SetupServer
  extend self

  attr_reader :server_read, :server_write, :server_err_read, :server_err_write

  LOGGER = begin
    default_name = ENV["CI"] ? "DEBUG" : "WARN"
    level_name = ENV["GITLAB_TESTING_LOG_LEVEL"]&.upcase
    level = Logger.const_get(level_name || default_name, true)
    Logger.new($stdout, level: level, formatter: ->(_, _, _, msg) { msg })
  end

  def root_path(*args)
    File.join(__dir__, "../../..", *args)
  end

  def cmd_args(*args)
    [root_path("topology-service"), *args]
  end

  def env
    {}
  end

  def read_server_out_nonblock(length = 1024)
    server_write.flush
    # Add a small delay in case we read too early before any asynchronous
    # operations are done. This is not guaranteed to read something but
    # it's a cheap way to increase our confidence without adding too much
    # code and latency.
    sleep(0.001)
    server_read.read_nonblock(length, exception: false)
  end

  def config_auth_method
    ENV.fetch("CONFIG_AUTH_METHOD", "mtls")
  end

  def test_with_tls_or_mtls?
    %w[tls mtls].include?(config_auth_method)
  end

  def test_with_mtls?
    %w[mtls].include?(config_auth_method)
  end

  def service_credentials
    return :this_channel_is_insecure unless test_with_tls_or_mtls?

    GRPC::Core::ChannelCredentials.new(
      File.read(root_path("tmp", "certs", "ca-cert.pem")),
      (File.read(root_path("tmp", "certs", "client-key.pem")) if test_with_mtls?),
      (File.read(root_path("tmp", "certs", "client-cert.pem")) if test_with_mtls?)
    )
  end

  def service_client(service_module)
    service_module::Stub.new("localhost:9097", service_credentials)
  end

  def http_client
    http = Net::HTTP.new("localhost", 9098)

    if test_with_tls_or_mtls?
      http.use_ssl = true
      http.ca_file = root_path("tmp", "certs", "ca-cert.pem")
      if test_with_mtls?
        http.cert = OpenSSL::X509::Certificate.new(File.read(root_path("tmp", "certs", "client-cert.pem")))
        http.key = OpenSSL::PKey::RSA.new(File.read(root_path("tmp", "certs", "client-key.pem")))
      end
    end

    http
  end

  def start
    raise ArgumentError, "The CONFIG_FILE is not set to run topology-service" unless ENV["CONFIG_FILE"]

    @server_read, @server_write = IO.pipe
    @server_err_read, @server_err_write = IO.pipe
    cmd = cmd_args("serve")
    @pid = spawn(env, *cmd, chdir: root_path, out: @server_write, err: @server_err_write)

    retry_n(:topology_service, 20, 0.1) do
      health_service = service_client(Gitlab::Cells::TopologyService::HealthService)
      health_service.readiness_probe(
        Gitlab::Cells::TopologyService::ReadinessProbeRequest.new
      )
    end

    Kernel.at_exit do
      stop
    end
  rescue # rubocop:disable Style/RescueStandardError
    stop
    raise
  end

  def stop
    signal_and_wait("TERM")
    signal_and_wait("KILL")
    @pid = nil
  end

  def signal_and_wait(signal)
    return unless @pid

    # The process can already be gone if the test run was INTerrupted.
    begin
      Process.kill(signal, @pid)
      Timeout.timeout(5) do
        Process.wait(@pid)
      rescue Timeout::Error
      end
    rescue Errno::ESRCH
    end
  end

  def retry_n(what, ntimes, delay, &_blk)
    LOGGER.debug "Trying to connect to #{what}: "
    last_e = nil

    ntimes.times do
      if yield
        LOGGER.debug " OK\n"
        return
      end
    rescue Errno::ENOENT, Errno::ECONNREFUSED, GRPC::Unavailable => e
      LOGGER.debug "."
      sleep delay
      last_e = e
    end

    LOGGER.warn " FAILED to connect to #{what}: #{last_e}\n"
    raise "could not connect to #{what}"
  end
end

RSpec.configure do |config|
  config.before(:suite) do
    SetupServer.start
  end

  config.before do
    SetupServer.read_server_out_nonblock(8192)
  end

  config.after(:suite) do
    SetupServer.stop
  end
end
