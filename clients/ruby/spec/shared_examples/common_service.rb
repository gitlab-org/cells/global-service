# frozen_string_literal: true

RSpec.shared_examples "a common service" do
  subject(:service) { SetupServer.service_client(described_class) }

  it "#get_cells" do
    req = Gitlab::Cells::TopologyService::GetCellsRequest.new
    res = service.get_cells(req)
    expect(res).not_to be_nil
  end
end
