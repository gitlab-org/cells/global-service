# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::Cells::TopologyService::HealthService do
  subject(:service) { SetupServer.service_client(described_class) }

  it ".liveness_probe" do
    req = Gitlab::Cells::TopologyService::LivenessProbeRequest.new
    res = service.liveness_probe(req)
    expect(res).not_to be_nil
  end

  it ".readiness_probe" do
    req = Gitlab::Cells::TopologyService::ReadinessProbeRequest.new
    res = service.readiness_probe(req)
    expect(res).not_to be_nil
  end

  it "GET /v1/health/liveness" do
    res = SetupServer.http_client.get("/v1/health/liveness")
    expect(res).to be_instance_of(Net::HTTPOK)
  end

  it "GET /v1/health/readiness" do
    res = SetupServer.http_client.get("/v1/health/readiness")
    expect(res).to be_instance_of(Net::HTTPOK)
  end
end
