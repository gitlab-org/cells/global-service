# frozen_string_literal: true

require "spec_helper"
require "json"

require_relative "../../../shared_examples/common_service"

RSpec.describe Gitlab::Cells::TopologyService::ClassifyService do
  include_examples "a common service"

  describe "GET /v1/cells" do
    it "#get_cells" do
      res = SetupServer.http_client.get("/v1/cells")
      expect(res).to be_instance_of(Net::HTTPOK)
    end
  end

  describe "POST /v1/classify" do
    context "when no type is passed" do
      it "returns error" do
        res = SetupServer.http_client.post("/v1/classify", "")
        expect(res).to be_instance_of(Net::HTTPBadRequest)
        expect(res.body).to match(/UNSPECIFIED/)
      end
    end

    context "when type is `FIRST_CELL`" do
      it "returns 200 OK with correct headers" do
        res = SetupServer.http_client.post("/v1/classify", { type: "FIRST_CELL" }.to_json)
        expect(res).to be_instance_of(Net::HTTPOK)

        expect(res.header["cache-control"]).to eq("s-maxage=60")
        expect(res.header["cache-tag"]).to eq("gprd_topology_service_gitlab_com")
      end
    end

    context "when type is `SESSION_PREFIX`" do
      context "when the value matches a cell" do
        it "returns 200 OK with correct headers" do
          res = SetupServer.http_client.post("/v1/classify", { type: "SESSION_PREFIX", value: "cell-1" }.to_json)
          expect(res).to be_instance_of(Net::HTTPOK)
          expect(JSON.parse(res.body)).to eq(
            { "action" => "PROXY", "proxy" => { "address" => "my.cell-1.example.com" } }
          )
          expect(res.header["cache-control"]).to eq("s-maxage=60")
          expect(res.header["cache-tag"]).to eq("gprd_topology_service_gitlab_com")
        end
      end

      context "when the value does not match any cell" do
        it "returns 404 Not Found" do
          res = SetupServer.http_client.post("/v1/classify", { type: "SESSION_PREFIX", value: "wrong_value" }.to_json)
          expect(res).to be_instance_of(Net::HTTPNotFound)
        end
      end
    end

    context "when type is `CELL_ID`" do
      context "when the value matches a cell" do
        it "returns 200 OK with correct headers" do
          res = SetupServer.http_client.post("/v1/classify", { type: "CELL_ID", value: "1" }.to_json)
          expect(res).to be_instance_of(Net::HTTPOK)
          expect(JSON.parse(res.body)).to eq(
            { "action" => "PROXY", "proxy" => { "address" => "my.cell-1.example.com" } }
          )
          expect(res.header["cache-control"]).to eq("s-maxage=60")
          expect(res.header["cache-tag"]).to eq("gprd_topology_service_gitlab_com")
        end
      end

      context "when the value does not match any cell" do
        it "returns 404 Not Found" do
          res = SetupServer.http_client.post("/v1/classify", { type: "CELL_ID", value: "420" }.to_json)
          expect(res).to be_instance_of(Net::HTTPNotFound)
        end
      end

      context "when the value is invalid" do
        it "returns 400 Bad Request" do
          res = SetupServer.http_client.post("/v1/classify", { type: "CELL_ID", value: "wrong_value" }.to_json)
          expect(res).to be_instance_of(Net::HTTPBadRequest)
        end
      end
    end
  end

  describe "#classify" do
    let(:service) { SetupServer.service_client(described_class) }

    context "when type is `FIRST_CELL`" do
      let(:req) { Gitlab::Cells::TopologyService::ClassifyRequest.new(type: "FIRST_CELL") }

      it "returns response" do
        response = service.classify(req)

        server_out = SetupServer.read_server_out_nonblock

        expect(server_out).to include(%("msg":"Classify"))
        expect(server_out).to include(%("type":1))

        expect(response.action).to eq(:PROXY)
        expect(response.proxy.address).to eq("my.cell-1.example.com")
      end
    end

    context "when type is `SESSION_PREFIX`" do
      let(:req) { Gitlab::Cells::TopologyService::ClassifyRequest.new(type: "SESSION_PREFIX", value: value) }

      context "when the value matches a cell" do
        let(:value) { "cell-1" }

        it "returns response" do
          response = service.classify(req)

          server_out = SetupServer.read_server_out_nonblock

          expect(server_out).to include(%("msg":"Classify"))
          expect(server_out).to include(%("type":2))
          expect(server_out).to include(%("value":"cell-1"))

          expect(response.action).to eq(:PROXY)
          expect(response.proxy.address).to eq("my.cell-1.example.com")
        end
      end

      context "when the value does not match any cell" do
        let(:value) { "wrong_value" }

        it "returns error" do
          expect { service.classify(req) }.to raise_error(GRPC::NotFound)
        end
      end
    end

    context "when type is `CELL_ID`" do
      let(:req) { Gitlab::Cells::TopologyService::ClassifyRequest.new(type: "CELL_ID", value: value) }

      context "when the value matches a cell" do
        let(:value) { "1" }

        it "returns response" do
          response = service.classify(req)

          server_out = SetupServer.read_server_out_nonblock

          expect(server_out).to include(%("msg":"Classify"))
          expect(server_out).to include(%("type":3))
          expect(server_out).to include(%("value":"1"))

          expect(response.action).to eq(:PROXY)
          expect(response.proxy.address).to eq("my.cell-1.example.com")
        end
      end

      context "when the value does not match any cell" do
        let(:value) { "420" }

        it "returns error" do
          expect { service.classify(req) }.to raise_error(GRPC::NotFound)
        end
      end

      context "when the value is invalid" do
        let(:value) { "wrong_value" }

        it "returns error" do
          expect { service.classify(req) }.to raise_error(GRPC::InvalidArgument)
        end
      end
    end
  end
end
