# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::Cells::TopologyService do # rubocop:disable RSpec/SpecFilePathFormat
  it "GET /metrics" do
    res = SetupServer.http_client.get("/metrics")
    expect(res).to be_instance_of(Net::HTTPOK)
    expect(res.body).to match(/promhttp_metric_handler_requests_total/)
  end
end
