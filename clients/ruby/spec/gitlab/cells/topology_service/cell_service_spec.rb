# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::Cells::TopologyService::CellService do
  describe "#get_cell" do
    let(:service) { SetupServer.service_client(described_class) }
    let(:get_cell_request) do
      Gitlab::Cells::TopologyService::GetCellRequest.new(cell_id: cell_id)
    end

    context "with cell-id passed in the param" do
      let(:cell_id) { 1 }

      it "returns the cell info with sequence range" do
        get_cell_response = service.get_cell(get_cell_request)

        server_out = SetupServer.read_server_out_nonblock

        expect(server_out).to include(%("msg":"GetCell"))
        expect(server_out).to include(%("id":#{cell_id}))

        expect(get_cell_response).to be_a Gitlab::Cells::TopologyService::GetCellResponse
        expect(get_cell_response.cell_info).not_to be_nil
        expect(get_cell_response.cell_info.sequence_range).to be_a Gitlab::Cells::TopologyService::SequenceRange
      end
    end
  end
end
