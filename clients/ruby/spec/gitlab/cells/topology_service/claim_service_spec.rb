# frozen_string_literal: true

require "spec_helper"

require_relative "../../../shared_examples/common_service"

RSpec.describe Gitlab::Cells::TopologyService::ClaimService do
  include_examples "a common service"

  describe "#create_claim" do
    let(:service) { SetupServer.service_client(described_class) }
    let(:unknown_bucket) { Gitlab::Cells::TopologyService::ClaimRecord::Bucket::UNSPECIFIED }
    let(:unknown_model) { Gitlab::Cells::TopologyService::ParentRecord::ApplicationModel::UNSPECIFIED }
    let(:unknown_table) { Gitlab::Cells::TopologyService::OwnerRecord::Table::UNSPECIFIED }

    let(:create_claim_request) do
      Gitlab::Cells::TopologyService::CreateClaimRequest.new(details: claim_details)
    end

    let(:valid_claim_details) do
      {
        claim: {
          bucket: Gitlab::Cells::TopologyService::ClaimRecord::Bucket::ROUTES,
          value: "gitlab-org/gitlab"
        },
        parent: {
          model: Gitlab::Cells::TopologyService::ParentRecord::ApplicationModel::PROJECT,
          id: 1
        },
        owner: {
          table: Gitlab::Cells::TopologyService::OwnerRecord::Table::ROUTES,
          id: 3
        }
      }
    end

    context "when passing a valid claim details object" do
      let(:claim_details) do
        Gitlab::Cells::TopologyService::ClaimDetails.new(valid_claim_details)
      end

      it "returns response containing claim information" do
        create_claim_response = service.create_claim(create_claim_request)

        server_out = SetupServer.read_server_out_nonblock

        expect(server_out).to include(%("msg":"CreateClaim"))
        expect(server_out).to include(%("details":{))

        expect(create_claim_response).to be_a Gitlab::Cells::TopologyService::CreateClaimResponse
        expect(create_claim_response.claim.id).to be > 0
        expect(create_claim_response.claim.cell_info).not_to be_nil
        expect(create_claim_response.claim.details).to eq(claim_details)
      end
    end
  end
end
