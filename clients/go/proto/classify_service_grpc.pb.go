// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.5.1
// - protoc             (unknown)
// source: proto/classify_service.proto

package proto

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.64.0 or later.
const _ = grpc.SupportPackageIsVersion9

const (
	ClassifyService_GetCells_FullMethodName = "/gitlab.cells.topology_service.ClassifyService/GetCells"
	ClassifyService_Classify_FullMethodName = "/gitlab.cells.topology_service.ClassifyService/Classify"
)

// ClassifyServiceClient is the client API for ClassifyService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
//
// Public read-only service used by various Routing Services
type ClassifyServiceClient interface {
	GetCells(ctx context.Context, in *GetCellsRequest, opts ...grpc.CallOption) (*GetCellsResponse, error)
	Classify(ctx context.Context, in *ClassifyRequest, opts ...grpc.CallOption) (*ClassifyResponse, error)
}

type classifyServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewClassifyServiceClient(cc grpc.ClientConnInterface) ClassifyServiceClient {
	return &classifyServiceClient{cc}
}

func (c *classifyServiceClient) GetCells(ctx context.Context, in *GetCellsRequest, opts ...grpc.CallOption) (*GetCellsResponse, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(GetCellsResponse)
	err := c.cc.Invoke(ctx, ClassifyService_GetCells_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *classifyServiceClient) Classify(ctx context.Context, in *ClassifyRequest, opts ...grpc.CallOption) (*ClassifyResponse, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(ClassifyResponse)
	err := c.cc.Invoke(ctx, ClassifyService_Classify_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ClassifyServiceServer is the server API for ClassifyService service.
// All implementations must embed UnimplementedClassifyServiceServer
// for forward compatibility.
//
// Public read-only service used by various Routing Services
type ClassifyServiceServer interface {
	GetCells(context.Context, *GetCellsRequest) (*GetCellsResponse, error)
	Classify(context.Context, *ClassifyRequest) (*ClassifyResponse, error)
	mustEmbedUnimplementedClassifyServiceServer()
}

// UnimplementedClassifyServiceServer must be embedded to have
// forward compatible implementations.
//
// NOTE: this should be embedded by value instead of pointer to avoid a nil
// pointer dereference when methods are called.
type UnimplementedClassifyServiceServer struct{}

func (UnimplementedClassifyServiceServer) GetCells(context.Context, *GetCellsRequest) (*GetCellsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCells not implemented")
}
func (UnimplementedClassifyServiceServer) Classify(context.Context, *ClassifyRequest) (*ClassifyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Classify not implemented")
}
func (UnimplementedClassifyServiceServer) mustEmbedUnimplementedClassifyServiceServer() {}
func (UnimplementedClassifyServiceServer) testEmbeddedByValue()                         {}

// UnsafeClassifyServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ClassifyServiceServer will
// result in compilation errors.
type UnsafeClassifyServiceServer interface {
	mustEmbedUnimplementedClassifyServiceServer()
}

func RegisterClassifyServiceServer(s grpc.ServiceRegistrar, srv ClassifyServiceServer) {
	// If the following call pancis, it indicates UnimplementedClassifyServiceServer was
	// embedded by pointer and is nil.  This will cause panics if an
	// unimplemented method is ever invoked, so we test this at initialization
	// time to prevent it from happening at runtime later due to I/O.
	if t, ok := srv.(interface{ testEmbeddedByValue() }); ok {
		t.testEmbeddedByValue()
	}
	s.RegisterService(&ClassifyService_ServiceDesc, srv)
}

func _ClassifyService_GetCells_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCellsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ClassifyServiceServer).GetCells(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: ClassifyService_GetCells_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ClassifyServiceServer).GetCells(ctx, req.(*GetCellsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ClassifyService_Classify_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ClassifyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ClassifyServiceServer).Classify(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: ClassifyService_Classify_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ClassifyServiceServer).Classify(ctx, req.(*ClassifyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// ClassifyService_ServiceDesc is the grpc.ServiceDesc for ClassifyService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var ClassifyService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "gitlab.cells.topology_service.ClassifyService",
	HandlerType: (*ClassifyServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetCells",
			Handler:    _ClassifyService_GetCells_Handler,
		},
		{
			MethodName: "Classify",
			Handler:    _ClassifyService_Classify_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/classify_service.proto",
}
