// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.36.3
// 	protoc        (unknown)
// source: proto/cell_service.proto

package proto

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type GetCellRequest struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	CellId        int64                  `protobuf:"varint,2,opt,name=cell_id,json=cellId,proto3" json:"cell_id,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *GetCellRequest) Reset() {
	*x = GetCellRequest{}
	mi := &file_proto_cell_service_proto_msgTypes[0]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *GetCellRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetCellRequest) ProtoMessage() {}

func (x *GetCellRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_cell_service_proto_msgTypes[0]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetCellRequest.ProtoReflect.Descriptor instead.
func (*GetCellRequest) Descriptor() ([]byte, []int) {
	return file_proto_cell_service_proto_rawDescGZIP(), []int{0}
}

func (x *GetCellRequest) GetCellId() int64 {
	if x != nil {
		return x.CellId
	}
	return 0
}

type GetCellResponse struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	CellInfo      *CellInfo              `protobuf:"bytes,1,opt,name=cell_info,json=cellInfo,proto3" json:"cell_info,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *GetCellResponse) Reset() {
	*x = GetCellResponse{}
	mi := &file_proto_cell_service_proto_msgTypes[1]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *GetCellResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetCellResponse) ProtoMessage() {}

func (x *GetCellResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_cell_service_proto_msgTypes[1]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetCellResponse.ProtoReflect.Descriptor instead.
func (*GetCellResponse) Descriptor() ([]byte, []int) {
	return file_proto_cell_service_proto_rawDescGZIP(), []int{1}
}

func (x *GetCellResponse) GetCellInfo() *CellInfo {
	if x != nil {
		return x.CellInfo
	}
	return nil
}

var File_proto_cell_service_proto protoreflect.FileDescriptor

var file_proto_cell_service_proto_rawDesc = []byte{
	0x0a, 0x18, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x63, 0x65, 0x6c, 0x6c, 0x5f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1d, 0x67, 0x69, 0x74, 0x6c,
	0x61, 0x62, 0x2e, 0x63, 0x65, 0x6c, 0x6c, 0x73, 0x2e, 0x74, 0x6f, 0x70, 0x6f, 0x6c, 0x6f, 0x67,
	0x79, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x15, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x2f, 0x63, 0x65, 0x6c, 0x6c, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x22, 0x29, 0x0a, 0x0e, 0x47, 0x65, 0x74, 0x43, 0x65, 0x6c, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x17, 0x0a, 0x07, 0x63, 0x65, 0x6c, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x06, 0x63, 0x65, 0x6c, 0x6c, 0x49, 0x64, 0x22, 0x57, 0x0a, 0x0f, 0x47,
	0x65, 0x74, 0x43, 0x65, 0x6c, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x44,
	0x0a, 0x09, 0x63, 0x65, 0x6c, 0x6c, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x27, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x65, 0x6c, 0x6c, 0x73,
	0x2e, 0x74, 0x6f, 0x70, 0x6f, 0x6c, 0x6f, 0x67, 0x79, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2e, 0x43, 0x65, 0x6c, 0x6c, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x08, 0x63, 0x65, 0x6c, 0x6c,
	0x49, 0x6e, 0x66, 0x6f, 0x32, 0x79, 0x0a, 0x0b, 0x43, 0x65, 0x6c, 0x6c, 0x53, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x12, 0x6a, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x43, 0x65, 0x6c, 0x6c, 0x12, 0x2d,
	0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x65, 0x6c, 0x6c, 0x73, 0x2e, 0x74, 0x6f,
	0x70, 0x6f, 0x6c, 0x6f, 0x67, 0x79, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47,
	0x65, 0x74, 0x43, 0x65, 0x6c, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x2e, 0x2e,
	0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x65, 0x6c, 0x6c, 0x73, 0x2e, 0x74, 0x6f, 0x70,
	0x6f, 0x6c, 0x6f, 0x67, 0x79, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47, 0x65,
	0x74, 0x43, 0x65, 0x6c, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42,
	0xd1, 0x01, 0x0a, 0x21, 0x63, 0x6f, 0x6d, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63,
	0x65, 0x6c, 0x6c, 0x73, 0x2e, 0x74, 0x6f, 0x70, 0x6f, 0x6c, 0x6f, 0x67, 0x79, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x42, 0x10, 0x43, 0x65, 0x6c, 0x6c, 0x53, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x50, 0x72, 0x6f, 0x74, 0x6f, 0x50, 0x01, 0x5a, 0x08, 0x2e, 0x2e, 0x2f, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0xa2, 0x02, 0x03, 0x47, 0x43, 0x54, 0xaa, 0x02, 0x1c, 0x47, 0x69, 0x74, 0x6c,
	0x61, 0x62, 0x2e, 0x43, 0x65, 0x6c, 0x6c, 0x73, 0x2e, 0x54, 0x6f, 0x70, 0x6f, 0x6c, 0x6f, 0x67,
	0x79, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0xca, 0x02, 0x1c, 0x47, 0x69, 0x74, 0x6c, 0x61,
	0x62, 0x5c, 0x43, 0x65, 0x6c, 0x6c, 0x73, 0x5c, 0x54, 0x6f, 0x70, 0x6f, 0x6c, 0x6f, 0x67, 0x79,
	0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0xe2, 0x02, 0x28, 0x47, 0x69, 0x74, 0x6c, 0x61, 0x62,
	0x5c, 0x43, 0x65, 0x6c, 0x6c, 0x73, 0x5c, 0x54, 0x6f, 0x70, 0x6f, 0x6c, 0x6f, 0x67, 0x79, 0x53,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x5c, 0x47, 0x50, 0x42, 0x4d, 0x65, 0x74, 0x61, 0x64, 0x61,
	0x74, 0x61, 0xea, 0x02, 0x1e, 0x47, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x3a, 0x3a, 0x43, 0x65, 0x6c,
	0x6c, 0x73, 0x3a, 0x3a, 0x54, 0x6f, 0x70, 0x6f, 0x6c, 0x6f, 0x67, 0x79, 0x53, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_proto_cell_service_proto_rawDescOnce sync.Once
	file_proto_cell_service_proto_rawDescData = file_proto_cell_service_proto_rawDesc
)

func file_proto_cell_service_proto_rawDescGZIP() []byte {
	file_proto_cell_service_proto_rawDescOnce.Do(func() {
		file_proto_cell_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_proto_cell_service_proto_rawDescData)
	})
	return file_proto_cell_service_proto_rawDescData
}

var file_proto_cell_service_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_proto_cell_service_proto_goTypes = []any{
	(*GetCellRequest)(nil),  // 0: gitlab.cells.topology_service.GetCellRequest
	(*GetCellResponse)(nil), // 1: gitlab.cells.topology_service.GetCellResponse
	(*CellInfo)(nil),        // 2: gitlab.cells.topology_service.CellInfo
}
var file_proto_cell_service_proto_depIdxs = []int32{
	2, // 0: gitlab.cells.topology_service.GetCellResponse.cell_info:type_name -> gitlab.cells.topology_service.CellInfo
	0, // 1: gitlab.cells.topology_service.CellService.GetCell:input_type -> gitlab.cells.topology_service.GetCellRequest
	1, // 2: gitlab.cells.topology_service.CellService.GetCell:output_type -> gitlab.cells.topology_service.GetCellResponse
	2, // [2:3] is the sub-list for method output_type
	1, // [1:2] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_proto_cell_service_proto_init() }
func file_proto_cell_service_proto_init() {
	if File_proto_cell_service_proto != nil {
		return
	}
	file_proto_cell_info_proto_init()
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_proto_cell_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_proto_cell_service_proto_goTypes,
		DependencyIndexes: file_proto_cell_service_proto_depIdxs,
		MessageInfos:      file_proto_cell_service_proto_msgTypes,
	}.Build()
	File_proto_cell_service_proto = out.File
	file_proto_cell_service_proto_rawDesc = nil
	file_proto_cell_service_proto_goTypes = nil
	file_proto_cell_service_proto_depIdxs = nil
}
