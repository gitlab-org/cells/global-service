// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.5.1
// - protoc             (unknown)
// source: proto/claim_service.proto

package proto

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.64.0 or later.
const _ = grpc.SupportPackageIsVersion9

const (
	ClaimService_GetCells_FullMethodName    = "/gitlab.cells.topology_service.ClaimService/GetCells"
	ClaimService_CreateClaim_FullMethodName = "/gitlab.cells.topology_service.ClaimService/CreateClaim"
)

// ClaimServiceClient is the client API for ClaimService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
//
// Restricted read-write service to claim global uniqueness on resources
type ClaimServiceClient interface {
	GetCells(ctx context.Context, in *GetCellsRequest, opts ...grpc.CallOption) (*GetCellsResponse, error)
	CreateClaim(ctx context.Context, in *CreateClaimRequest, opts ...grpc.CallOption) (*CreateClaimResponse, error)
}

type claimServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewClaimServiceClient(cc grpc.ClientConnInterface) ClaimServiceClient {
	return &claimServiceClient{cc}
}

func (c *claimServiceClient) GetCells(ctx context.Context, in *GetCellsRequest, opts ...grpc.CallOption) (*GetCellsResponse, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(GetCellsResponse)
	err := c.cc.Invoke(ctx, ClaimService_GetCells_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *claimServiceClient) CreateClaim(ctx context.Context, in *CreateClaimRequest, opts ...grpc.CallOption) (*CreateClaimResponse, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(CreateClaimResponse)
	err := c.cc.Invoke(ctx, ClaimService_CreateClaim_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ClaimServiceServer is the server API for ClaimService service.
// All implementations must embed UnimplementedClaimServiceServer
// for forward compatibility.
//
// Restricted read-write service to claim global uniqueness on resources
type ClaimServiceServer interface {
	GetCells(context.Context, *GetCellsRequest) (*GetCellsResponse, error)
	CreateClaim(context.Context, *CreateClaimRequest) (*CreateClaimResponse, error)
	mustEmbedUnimplementedClaimServiceServer()
}

// UnimplementedClaimServiceServer must be embedded to have
// forward compatible implementations.
//
// NOTE: this should be embedded by value instead of pointer to avoid a nil
// pointer dereference when methods are called.
type UnimplementedClaimServiceServer struct{}

func (UnimplementedClaimServiceServer) GetCells(context.Context, *GetCellsRequest) (*GetCellsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCells not implemented")
}
func (UnimplementedClaimServiceServer) CreateClaim(context.Context, *CreateClaimRequest) (*CreateClaimResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateClaim not implemented")
}
func (UnimplementedClaimServiceServer) mustEmbedUnimplementedClaimServiceServer() {}
func (UnimplementedClaimServiceServer) testEmbeddedByValue()                      {}

// UnsafeClaimServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ClaimServiceServer will
// result in compilation errors.
type UnsafeClaimServiceServer interface {
	mustEmbedUnimplementedClaimServiceServer()
}

func RegisterClaimServiceServer(s grpc.ServiceRegistrar, srv ClaimServiceServer) {
	// If the following call pancis, it indicates UnimplementedClaimServiceServer was
	// embedded by pointer and is nil.  This will cause panics if an
	// unimplemented method is ever invoked, so we test this at initialization
	// time to prevent it from happening at runtime later due to I/O.
	if t, ok := srv.(interface{ testEmbeddedByValue() }); ok {
		t.testEmbeddedByValue()
	}
	s.RegisterService(&ClaimService_ServiceDesc, srv)
}

func _ClaimService_GetCells_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCellsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ClaimServiceServer).GetCells(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: ClaimService_GetCells_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ClaimServiceServer).GetCells(ctx, req.(*GetCellsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ClaimService_CreateClaim_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateClaimRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ClaimServiceServer).CreateClaim(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: ClaimService_CreateClaim_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ClaimServiceServer).CreateClaim(ctx, req.(*CreateClaimRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// ClaimService_ServiceDesc is the grpc.ServiceDesc for ClaimService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var ClaimService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "gitlab.cells.topology_service.ClaimService",
	HandlerType: (*ClaimServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetCells",
			Handler:    _ClaimService_GetCells_Handler,
		},
		{
			MethodName: "CreateClaim",
			Handler:    _ClaimService_CreateClaim_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/claim_service.proto",
}
