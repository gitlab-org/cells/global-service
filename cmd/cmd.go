package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/gitlab-org/cells/topology-service/cmd/serve"
)

var RootCmd = &cobra.Command{
	Use:               "topology-service",
	Short:             "Topology Service is service to provide common functions for GitLab Cells",
	PersistentPreRunE: loadConfig,
}

func init() {
	RootCmd.PersistentFlags().String("config", defaultConfigFile(), "Configuration file to use.")
	RootCmd.AddCommand(serve.Cmd)
}
