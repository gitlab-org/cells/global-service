package serve

import (
	"context"
	"errors"
	"sync/atomic"
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"go.uber.org/goleak"

	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
)

func Test_RunsManyServeConfigs(t *testing.T) {
	ctx, cancelCause := context.WithCancelCause(context.Background())
	defer cancelCause(nil)

	triggerErr := errors.New("trigger")

	c := &config.Config{
		Serve: []config.ServeConfig{
			*testServeConfig("*_grpc"),
			*testServeConfig("*_rest"),
		},
	}

	go func() {
		// give one second for all to start
		time.Sleep(time.Second)
		cancelCause(triggerErr)
	}()

	canceled := int32(0)

	err := runWithConfig(ctx, c, func(err error) {
		atomic.AddInt32(&canceled, 1)
		cancelCause(err)
	})
	assert.Equal(t, triggerErr, err)
	assert.Equal(t, int32(len(c.Serve)), atomic.LoadInt32(&canceled), "all controllers gracefully exited")
}

func Test_InterruptsWithSignal(t *testing.T) {
	ctx, cancelCause := context.WithCancelCause(context.Background())
	defer cancelCause(nil)

	c := &config.Config{
		Serve: []config.ServeConfig{
			*testServeConfig("*_grpc"),
		},
	}

	// Trigger interrupt when ready
	ctxWithCallback := context.WithValue(ctx, "runReadyCb", func() {
		syscall.Kill(syscall.Getpid(), syscall.SIGINT)
	})

	err := runWithConfig(ctxWithCallback, c, cancelCause)
	assert.Equal(t, errors.New("interrupt"), err)
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
