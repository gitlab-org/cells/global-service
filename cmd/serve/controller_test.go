package serve

import (
	"context"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
)

func resetRegistry() {
	prometheus.DefaultRegisterer = prometheus.NewRegistry()
}

func testServeConfig(features ...string) *config.ServeConfig {
	return &config.ServeConfig{
		Address:                "localhost:0",
		CertificateFile:        "../../tmp/certs/server-cert.pem",
		KeyFile:                "../../tmp/certs/server-key.pem",
		ClientCertificateFiles: []string{"../../tmp/certs/ca-cert.pem"},
		Features:               features,
	}
}

func TestController_RunsAllServices(t *testing.T) {
	for _, feature := range config.AllFeatures {
		t.Run(string(feature), func(t *testing.T) {
			c := &controller{}
			err := c.Open(
				context.Background(),
				&config.Config{},
				testServeConfig(string(feature)),
			)
			require.NoError(t, err)
			defer c.Close()

			if feature.IsGRPC() {
				require.NotNil(t, c.grpc)
				assert.Len(t, c.grpc.GetServiceInfo(), 1)
				assert.Nil(t, c.mux)
			} else if feature.IsREST() {
				require.NotNil(t, c.mux)
				assert.Nil(t, c.grpc)
			} else {
				t.Fatalf("unknown feature: %s", feature)
			}
		})
	}
}

func TestController_gRPC_GracefullyStops(t *testing.T) {
	c := &controller{}
	err := c.Open(
		context.Background(),
		&config.Config{},
		testServeConfig("*_grpc"),
	)
	require.NoError(t, err)

	c.Close()

	err = c.Run(context.Background())
	assert.EqualError(t, err, "grpc: the server has been stopped")
}

func TestController_REST_GracefullyStops(t *testing.T) {
	c := &controller{}
	err := c.Open(
		context.Background(),
		&config.Config{},
		testServeConfig("*_rest"),
	)
	require.NoError(t, err)

	c.Close()

	err = c.Run(context.Background())
	require.Error(t, err)
	assert.Contains(t, err.Error(), "use of closed network connection")
}
