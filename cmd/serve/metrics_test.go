package serve

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewMetricsHandlerFactoryReturnsSameInstance(t *testing.T) {
	factory := newMetricsHandlerFactory()
	if factory == nil {
		t.Error("Expected non-nil factory")
	}

	factory2 := newMetricsHandlerFactory()
	if factory != factory2 {
		t.Error("Expected subsequent calls to return the same instance")
	}
}

func TestRegisterMetricsEndpoint(t *testing.T) {
	c := &controller{}
	c.registerMetricsEndpoint()
	_, err := http.NewRequest("GET", "/metrics", nil)
	assert.NoError(t, err)
}
