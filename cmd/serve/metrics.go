package serve

import (
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/gitlab-org/labkit/metrics"
)

var metricsHandlerFactory *metrics.HandlerFactory

func newMetricsHandlerFactory() *metrics.HandlerFactory {
	if metricsHandlerFactory == nil {
		f := metrics.NewHandlerFactory(metrics.WithNamespace("topology_service"))
		metricsHandlerFactory = &f
	}
	return metricsHandlerFactory
}

// Register '/metrics' endpoint
func (c *controller) registerMetricsEndpoint() {
	promHandler := func(w http.ResponseWriter, r *http.Request, _ map[string]string) {
		promhttp.Handler().ServeHTTP(w, r)
	}
	pattern := runtime.MustPattern(runtime.NewPattern(1, []int{2, 0}, []string{"metrics"}, ""))
	c.ensureMux().Handle("GET", pattern, promHandler)
}
