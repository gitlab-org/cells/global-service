package serve

import (
	"net/http"

	"gitlab.com/gitlab-org/labkit/log"
)

func logMiddleware(handler http.Handler) http.Handler {
	return log.AccessLogger(handler,
		log.WithFieldsExcluded(
			log.HTTPProto|
				log.HTTPHost|
				log.HTTPRemoteIP|
				log.HTTPRemoteAddr|
				log.HTTPRequestMethod|
				log.HTTPRequestReferrer|
				log.HTTPUserAgent|
				log.HTTPResponseContentType),
	)
}
