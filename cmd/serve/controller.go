package serve

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/gitlab-org/cells/topology-service/clients/go/proto"
	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
	grpc_helpers "gitlab.com/gitlab-org/cells/topology-service/internal/helpers/grpc"
	cell_service "gitlab.com/gitlab-org/cells/topology-service/internal/services/cell"
	claim_service "gitlab.com/gitlab-org/cells/topology-service/internal/services/claim"
	classify_service "gitlab.com/gitlab-org/cells/topology-service/internal/services/classify"
	health_service "gitlab.com/gitlab-org/cells/topology-service/internal/services/health"
)

type controller struct {
	cfg       *config.Config
	serveCfg  *config.ServeConfig
	grpc      *grpc.Server
	mux       *runtime.ServeMux
	tlsConfig *tls.Config
	socket    net.Listener
}

func (c *controller) Addr() net.Addr {
	return c.socket.Addr()
}

func (c *controller) Open(ctx context.Context, cfg *config.Config, serveCfg *config.ServeConfig) error {
	if c.cfg != nil {
		return errors.New("already open")
	}
	c.cfg = cfg
	c.serveCfg = serveCfg

	if tlsConfig, err := c.serveCfg.TlsConfig(); err != nil {
		return err
	} else {
		c.tlsConfig = tlsConfig
	}

	if socket, err := c.serveCfg.Socket(); err != nil {
		return err
	} else {
		c.socket = socket
	}

	// Health Service
	if c.serveCfg.HasFeature(config.ServeFeature_HealthService_REST) {
		if err := proto.RegisterHealthServiceHandlerServer(ctx, c.ensureMux(), health_service.New(c.cfg)); err != nil {
			return err
		}
	}
	if c.serveCfg.HasFeature(config.ServeFeature_HealthService_gRPC) {
		proto.RegisterHealthServiceServer(c.ensureGrpc(), health_service.New(c.cfg))
	}

	// Classify Service
	if c.serveCfg.HasFeature(config.ServeFeature_ClassifyService_REST) {
		if err := proto.RegisterClassifyServiceHandlerServer(ctx, c.ensureMux(), classify_service.New(c.cfg)); err != nil {
			return err
		}
	}
	if c.serveCfg.HasFeature(config.ServeFeature_ClassifyService_gRPC) {
		proto.RegisterClassifyServiceServer(c.ensureGrpc(), classify_service.New(c.cfg))
	}
	if c.serveCfg.HasFeature(config.ServeFeature_CellService_gRPC) {
		proto.RegisterCellServiceServer(c.ensureGrpc(), cell_service.New(c.cfg))
	}

	// Claim Service
	if c.serveCfg.HasFeature(config.ServeFeature_ClaimService_gRPC) {
		proto.RegisterClaimServiceServer(c.ensureGrpc(), claim_service.New(c.cfg))
	}

	if c.mux != nil && c.grpc != nil {
		return errors.New("cannot run both gRPC and REST server")
	} else if c.mux == nil && c.grpc == nil {
		return errors.New("no servers to run")
	}

	// Metrics
	if c.serveCfg.HasFeature("*_rest") {
		c.registerMetricsEndpoint()
	}

	return nil
}

func (c *controller) ensureMux() *runtime.ServeMux {
	if c.mux == nil {
		c.mux = runtime.NewServeMux(
			runtime.WithOutgoingHeaderMatcher(grpc_helpers.RawHeaderMatcher),
		)
	}
	return c.mux
}

func (c *controller) ensureGrpc() *grpc.Server {
	if c.grpc == nil {
		options := []grpc.ServerOption{}
		if c.tlsConfig != nil {
			options = append(options, grpc.Creds(credentials.NewTLS(c.tlsConfig)))
		}
		c.grpc = grpc.NewServer(options...)
	}
	return c.grpc
}

func (c *controller) Run(_ context.Context) error {
	if c.mux != nil {
		fmt.Println("REST", c.serveCfg.Mode(), "server running at port", c.socket.Addr())

		socket := c.socket
		if c.tlsConfig != nil {
			socket = tls.NewListener(c.socket, c.tlsConfig)
		}

		metricsMiddleware := *newMetricsHandlerFactory()
		handler := logMiddleware(metricsMiddleware(c.mux))
		return http.Serve(socket, handler)
	}

	if c.grpc != nil {
		fmt.Println("gRPC", c.serveCfg.Mode(), "server running at port", c.socket.Addr())
		return c.grpc.Serve(c.socket)
	}

	return nil
}

func (c *controller) Close() {
	if c.grpc != nil {
		c.grpc.GracefulStop()
	}
	if c.socket != nil {
		c.socket.Close()
	}
}
