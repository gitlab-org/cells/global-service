package serve

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
)

var Cmd = &cobra.Command{
	Use:          "serve",
	Short:        "Run Topology Service server",
	Args:         cobra.ExactArgs(0),
	SilenceUsage: true,
	RunE:         run,
}

func runWithConfig(ctx context.Context, cfg *config.Config, cancelCause context.CancelCauseFunc) error {
	controllers := []*controller{}
	close := func() {
		for _, controller := range controllers {
			controller.Close()
		}
	}
	defer close()

	for _, serveCfg := range cfg.Serve {
		controller := &controller{}
		err := controller.Open(ctx, cfg, &serveCfg)
		if err != nil {
			return fmt.Errorf("failed to open controllers: %v", err)
		}
		controllers = append(controllers, controller)
	}

	wg := sync.WaitGroup{}

	for _, c := range controllers {
		wg.Add(1)
		go func(c *controller) {
			defer wg.Done()
			err := c.Run(ctx)
			cancelCause(err)
		}(c)
	}

	signalCh := make(chan os.Signal)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGTERM)
	defer signal.Stop(signalCh)

	// Test callback
	if cb, _ := ctx.Value("runReadyCb").(func()); cb != nil {
		cb()
	}

	select {
	case s := <-signalCh:
		cancelCause(errors.New(s.String()))

	case <-ctx.Done():
		// finished by controller exit
	}

	// trigger graceful shutdown and wait for all to exit
	close()
	wg.Wait()

	return context.Cause(ctx)
}

func run(cmd *cobra.Command, _ []string) error {
	ctx, cancelCause := context.WithCancelCause(cmd.Context())
	defer cancelCause(nil)

	cfg := ctx.Value("config").(*config.Config)

	return runWithConfig(ctx, cfg, cancelCause)
}
