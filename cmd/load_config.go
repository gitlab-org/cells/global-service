package cmd

import (
	"context"
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-org/cells/topology-service/internal/config"
)

func defaultConfigFile() string {
	configFile := os.Getenv("CONFIG_FILE")
	if configFile == "" {
		configFile = "config.toml"
	}
	return configFile
}

func loadConfig(cmd *cobra.Command, _ []string) error {
	cfg, err := config.ReadFile(cmd.Flag("config").Value.String())
	if err != nil {
		return fmt.Errorf("failed to read config: %v", err)
	}

	ctx := context.WithValue(cmd.Context(), "config", cfg)
	cmd.SetContext(ctx)
	return nil
}
