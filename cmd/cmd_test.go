package cmd_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/require"

	"go.uber.org/goleak"

	"gitlab.com/gitlab-org/cells/topology-service/cmd"
)

var errInterrupted = errors.New("interrupted")

func TestCmdLine(t *testing.T) {
	tests := map[string]struct {
		Args     []string
		Expected error
	}{
		"valid config via cmdline": {
			Args:     []string{"--config", "fixtures/config-valid.toml", "serve"},
			Expected: errInterrupted,
		},
		"without sequence range": {
			Args:     []string{"--config", "fixtures/config-without-cell-sequence.toml", "serve"},
			Expected: errors.New("failed to read config: cells[0]: cell sequence min limit should be less than max limit"),
		},
		"with minval great than maxval in sequence range": {
			Args:     []string{"--config", "fixtures/config-invalid-sequence.toml", "serve"},
			Expected: errors.New("failed to read config: cells[0]: cell sequence min limit should be less than max limit"),
		},
		"config not existing via cmdline": {
			Args:     []string{"--config", "non-existing-config.toml", "serve"},
			Expected: errors.New("failed to read config: open non-existing-config.toml: no such file or directory"),
		},
		"invalid config via cmdline": {
			Args:     []string{"--config", "fixtures/config-empty.toml", "serve"},
			Expected: errors.New("failed to read config: no cells in config file"),
		},
		"duplicate response headers config via cmdline": {
			Args:     []string{"--config", "fixtures/config-duplicate-headers.toml", "serve"},
			Expected: errors.New("failed to read config: services: classify: Duplicate key \"Cache-Tag\" for no codes"),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx, cancelCause := context.WithCancelCause(context.Background())
			ctxWithCallback := context.WithValue(ctx, "runReadyCb", func() {
				cancelCause(errInterrupted)
			})

			cmd.RootCmd.SetArgs(test.Args)
			err := cmd.RootCmd.ExecuteContext(ctxWithCallback)
			require.Equal(t, test.Expected, err)
		})
	}
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
