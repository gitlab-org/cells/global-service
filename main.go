package main

import (
	"fmt"
	"os"

	"gitlab.com/gitlab-org/cells/topology-service/cmd"
	"gitlab.com/gitlab-org/labkit/log"
)

func main() {
	closer, err := log.Initialize(
		log.WithOutputName("stdout"),
		log.WithFormatter("json"),
	)

	if err != nil {
		fmt.Println("Failed to initialize logger:", err)
		os.Exit(1)
	}
	defer closer.Close()

	err = cmd.RootCmd.Execute()
	if err != nil {
		fmt.Println("Failed to start Topology Service:", err)
		os.Exit(1)
	}
}
